# Overview

  This project is designed for use in Terminal.app on macOS/OS X, however it should also work with Bash and Zsh on Linux and Windows.
Is also bundled with two profiles for macOS's Terminal.app, "GAUSS" and "GAUSS Game" which have their fonts, typefaces, line and column spacing, and X-Term256
colour palettes preconfigured for drawing, and rendering images respectively.

  GAUSS uses UTF-16 characters for pixels, and supports four pixel denisties: solid (`${pX}`, UTF-16 `x25x88`), 25% dither (`${dthaX}`, UTF-16 `x25x91`), 
50% dither (`${dthbX}`, UTF-16 `x25x92`), and 75% dither (`${dthcX}`, UTF-16 `x25x93`) where X is the number of columns to fill. 
GAUSS also uses ANSI escape codes for colour, and expects a custom colour palette which differs from the standard found on existing UNIX distributions.
The Terminal.app profiles included posses this colour configuration, Linux or Windows users however will need to manually set their ANSI colour palette to match
that included in the Terminal.app profiles (found below).

  Colour support includes per-pixel background and foreground colour, which can be used in combination with per-pixel dithering to achieve a theoretical
colour depth of 1,024 colours. Commands can only be entered manually by user at a prompt, but images can be saved by piping the output to a file, which will
save the image in the form of raw UTF-16 characters and ANSI escape codes, displayable in any properly configured X-Term256 emulator. It is recommended
that you save your source script for easier editding of images, though it is entirely possible to edit a pre-rendered image given a text editor that displays
typically hidden formatting characters.

# Installation

**Mac OS X**

  Installation of GAUSS is fairly simple on macOS/OS X. Simply append the contents of the file "GAUSS Bindings" to your `.bash_profile`, and import the included
"GAUSS.terminal" and "GAUSS Game.terminal" files to your Terminal.app.

**Linux, Windows**

  Linux and Windows users should also install GAUSS by appending the contents of the file "GAUSS Bindings" to your `.bash_profile`, `.profile`, or `.bashrc`
file, however it is recommended they instead create two bash profiles with the following settings, and append "GAUSS Bindings" to each:

**GAUSS profile:**
>
>Background colour: `Black`  
>Text colour: `White`  
>Typeface: `Menlo Regular`  
>Font: `13 pt.`  
>Character Spacing: `1`  
>Line Spacing: `0.764`  
>Antialias text: `Yes`  
>Use bold fonts: `Yes`  
>Allow Blinking Text: `Yes`  
>Display ANSI colours: `Yes`  
>Use bright colours for bold text: `Yes`  
>

**GAUSS Game profile:**
>
>Background colour: `Black`  
>Text colour: `White`  
>Typeface: `SF Mono Regular`  
>Font: `9 pt.`  
>Character Spacing: `0.8`  
>Line Spacing: `0.74`  
>Antialias text: `Yes`  
>Use bold fonts: `Yes`  
>Allow blinking text: `Yes`  
>Display ANSI colours: `Yes`  
>Use bright colours for bold text: `Yes`  
>

# Colour Sets

  The following are the hex values of the respective colour commands used by GAUSS. ***If you use Linux or Windows you will need to manually adjust your ANSI
colour palette to match before drawing or rendering GAUSS images.*** It is recommended you add these colour palette settings to your dedicated GAUSS/GAUSS Game
bash profiles, as to not disrupt your main shell configuration.

**Foregrounds:**

>
>Black FG (`[38;5;0m`):`000000`  
>Dark Grey FG (`[38;5;8m`):`5E5E5E`  
>Pink FG (`[38;5;1m`):`FF85FF`  
>Red FG (`[38;5;9m`):`FF2600`  
>Green FG (`[38;5;2m`):`008F00`  
>Lime FG (`[38;5;10m`):`73FA79`  
>Orange FG (`[38;5;3m`):`FF9300`  
>Yellow FG (`[38;5;11m`):`FFFB00`  
>Indigo FG (`[38;5;4m`):`0433FF`  
>Blue FG (`[38;5;12m`):`0096FF`  
>Purple FG (`[38;5;5m`):`531B93`  
>Magenta FG (`[38;5;13m`):`FF40FF`  
>Teal FG (`[38;5;6m`):`009193`  
>Cyan FG (`[38;5;14m`):`73FDFF`  
>Light Grey FG (`[38;5;7m`):`C0C0C0`  
>Whtie FG (`[38;5;15m`):`EBEBEB`  
>

**Backgrounds:**

>
>Black FG (`[48;5;0m`):`000000`  
>Dark Grey FG (`[48;5;8m`):`5E5E5E`  
>Pink FG (`[48;5;1m`):`FF85FF`  
>Red FG (`[48;5;9m`):`FF2600`  
>Green FG (`[48;5;2m`):`008F00`  
>Lime FG (`[48;5;10m`):`73FA79`  
>Orange FG (`[48;5;3m`):`FF9300`  
>Yellow FG (`[48;5;11m`):`FFFB00`  
>Indigo FG (`[48;5;4m`):`0433FF`  
>Blue FG (`[48;5;12m`):`0096FF`  
>Purple FG (`[48;5;5m`):`531B93`  
>Magenta FG (`[48;5;13m`):`FF40FF`  
>Teal FG (`[48;5;6m`):`009193`  
>Cyan FG (`[48;5;14m`):`73FDFF`  
>Light Grey FG (`[48;5;7m`):`C0C0C0`  
>Whtie FG (`[48;5;15m`):`EBEBEB`  
>

# Example Images

  Here are some examples of images that were created using GAUSS, along with their respective scripts and RAWs.
Of the upper images, the ones in the centre are the original sprites, those on the left are rendered in the "GAUSS Game" profile, 
and the ones on the right are rendered in the "GAUSS" profile and zoomed in to show in more detail what the colour dithering looks like,
and how certain colours are achieved with the relatively limited 16-colour palette. Of those on the bottom, the left is drag-n-dropable
Bash script, the right is the raw UTF-16 & ANSI codes as they are saved to a file.

**Big Key** from **Legend of Zelda: A Link to the Past**  
![alt-text](ExampleImages/Example2.png)

**Big Key Source & RAW**  
![alt-text](ExampleImages/Example2RAWs.png)

**Boss Key** from **Legend of Zelda: The Minish Cap**  
![alt-text](ExampleImages/Example1.png)

**Boss Key Source & RAW**  
![alt-text](ExampleImages/Example1RAWs.png)

**Link** from **Legend of Zelda: A Link to the Past**  
![alt-text](ExampleImages/Example3.png)

**Link SNES Source & RAW**  
![alt-text](ExampleImages/Example3RAWs.png)

# Animations

  GAUSS can also be used to create animations by combining `clear`, `cat`, and `sleep` to call individually drawn frames in a script.
Below is an example animation, along with a snipet of the animation script that runs the 9 frame cycle.

**Door Opening Animation** from **Metroid**  
![alt-text](ExampleImages/MetroidDoor.gif)

**Door Opening Animation Script**  
![alt-text](ExampleImages/AnimationExample.png)